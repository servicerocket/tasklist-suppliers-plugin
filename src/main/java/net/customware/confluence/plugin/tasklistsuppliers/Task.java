package net.customware.confluence.plugin.tasklistsuppliers;

import java.util.Date;
import java.util.List;

public class Task {
    public enum Priority {
        LOW("L", "Low"), MEDIUM("M", "Medium"), HIGH("H", "High");

        Priority( String code, String label ) {
            this.code = code;
            this.label = label;
        }

        private final String code;

        private final String label;

        public String code() {
            return code;
        }

        public String label() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }

        public static Priority forCode( String code ) {
            for ( Priority p : Priority.values() ) {
                if ( p.code.equals( code ) )
                    return p;
            }
            return HIGH;
        }
    }

    private boolean completed;

    private Priority priority;

    private boolean locked;

    private Date createdDate;

    private Date completedDate;

    private List<String> assignees;

    private String name;

    public Task( boolean completed, Priority priority, boolean locked, Date createdDate, Date completedDate,
            List<String> assignees, String name ) {
        super();
        this.completed = completed;
        this.priority = priority;
        this.locked = locked;
        this.createdDate = createdDate;
        this.completedDate = completedDate;
        this.assignees = assignees;
        this.name = name;
    }

    public Task() {
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted( boolean completed ) {
        this.completed = completed;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority( Priority priority ) {
        this.priority = priority;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked( boolean locked ) {
        this.locked = locked;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate( Date createdDate ) {
        this.createdDate = createdDate;
    }

    public Date getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate( Date completedDate ) {
        this.completedDate = completedDate;
    }

    public List<String> getAssignees() {
        return assignees;
    }

    public void setAssignees( List<String> assignees ) {
        this.assignees = assignees;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }
}
