package net.customware.confluence.plugin.tasklistsuppliers;

import java.util.List;

import org.randombits.confluence.supplier.SimpleSupplier;
import org.randombits.confluence.supplier.SupplierException;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.spring.container.ContainerManager;

public class TaskSupplier extends SimpleSupplier<Task> {

    public static final String PREFIX = "task";

    private static final String NAME_KEY = "name";

    private static final String IS_COMPLETED_KEY = "is completed";

    private static final String PRIORITY_KEY = "priority";

    private static final String IS_LOCKED_KEY = "is locked";

    private static final String CREATED_DATE_KEY = "created date";

    private static final String COMPLETED_DATE_KEY = "completed date";

    private static final String ASSIGNEE_KEY = "assignee";

    private static final String ASSIGNEES_KEY = "assignees";

    private static final String IS_ASSIGNEE_PREFIX = "is assignee ";

    private static final String SELF_ASSIGNEE = "@self";

    private UserAccessor userAccessor;

    public TaskSupplier() {
        super( Task.class, false, PREFIX );
        ContainerManager.autowireComponent( this );
    }

    public void setUserAccessor( UserAccessor userAccessor ) {
        this.userAccessor = userAccessor;
    }

    @Override
    protected Object findValue( Task task, String key ) throws SupplierException {
        if ( key == null || NAME_KEY.equals( key ) )
            return task.getName();
        if ( IS_COMPLETED_KEY.equals( key ) )
            return task.isCompleted();
        if ( PRIORITY_KEY.equals( key ) )
            return task.getPriority().label();
        if ( IS_LOCKED_KEY.equals( key ) )
            return task.isLocked();
        if ( CREATED_DATE_KEY.equals( key ) )
            return task.getCreatedDate();
        if ( COMPLETED_DATE_KEY.equals( key ) )
            return task.getCompletedDate();
        if ( ASSIGNEE_KEY.equals( key ) || ASSIGNEES_KEY.equals( key ) )
            return task.getAssignees();
        if ( key.startsWith( IS_ASSIGNEE_PREFIX ) )
            return isAssignee( task, key.substring( IS_ASSIGNEE_PREFIX.length() ) );

        return null;
    }

    private Boolean isAssignee( Task task, String name ) {
        // First, check a direct name match in the assignee list.
        List<String> assignees = task.getAssignees();
        if ( assignees == null )
            return Boolean.FALSE;

        if ( SELF_ASSIGNEE.equals( name ) )
            name = AuthenticatedUserThreadLocal.getUsername();

        if ( assignees.contains( name ) )
            return true;

        // Check if any of the assignees are groups, and if so is the user is a
        // member.
        for ( String assignee : assignees ) {
            if ( userAccessor.hasMembership( assignee, name ) )
                return true;
        }

        return false;

    }
}
