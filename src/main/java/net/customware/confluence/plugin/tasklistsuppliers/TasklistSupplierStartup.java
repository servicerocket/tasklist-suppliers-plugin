package net.customware.confluence.plugin.tasklistsuppliers;

import org.randombits.confluence.intercom.ConnectionBundle;
import org.randombits.confluence.intercom.IntercomStartup;
import org.randombits.confluence.intercom.LocalIntercomListener;
import org.randombits.confluence.supplier.LocalSupplierBundle;

public class TasklistSupplierStartup extends IntercomStartup {

    @Override
    protected ConnectionBundle createConnectionBundle() {
        return new LocalSupplierBundle( new TasklistSupplier(), new TaskSupplier() );
    }

    @Override
    protected LocalIntercomListener createLocalIntercomListener() {
        return null;
    }
}
