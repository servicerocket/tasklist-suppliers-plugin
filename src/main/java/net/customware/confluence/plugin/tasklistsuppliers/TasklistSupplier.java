package net.customware.confluence.plugin.tasklistsuppliers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.randombits.confluence.supplier.SimpleSupplier;
import org.randombits.confluence.supplier.SupplierException;

import com.atlassian.confluence.core.ContentEntityObject;

public class TasklistSupplier extends SimpleSupplier<ContentEntityObject> {

    private static final String PREFIX = "tasklist";

    // Matches {tasklist} macros. Groups: 1: list name, 2. extra options, 3.
    // macro body
    private static final Pattern TASKLIST_PATTERN = Pattern.compile(
            "(?<!\\\\)\\{tasklist(?:\\:(?:([^\\=}]+(?=[\\|\\}]))[\\|]?)?([^\\}]+)?)?\\}(.*?)(?<!\\\\)\\{tasklist\\}",
            Pattern.DOTALL );

    private static final Pattern TASK_PATTERN = Pattern.compile( "(?<=\\|)([^\\|]+)" );

    private static final int NAME_GROUP = 1;

    private static final int OPTIONS_GROUP = 2;

    private static final int BODY_GROUP = 3;

    /**
     * The key to use to retrieve all task on the page from all tasklists.
     */
    public static final String ALL_KEY = "@all";

    public static final String DEFAULT_KEY = "@default";

    private static final String COMPLETED_HEADING = "completed";

    private static final String TRUE = "T";

    private static final String PRIORITY_HEADING = "priority";

    private static final String LOCKED_HEADING = "locked";

    private static final String CREATED_DATE_HEADING = "createddate";

    private static final String COMPLETED_DATE_HEADING = "completeddate";

    private static final String ASSIGNEE_HEADING = "assignee";

    private static final String NAME_HEADING = "name";

    public TasklistSupplier() {
        super( ContentEntityObject.class, true, PREFIX );
    }

    @Override
    protected Object findValue( ContentEntityObject context, String key ) throws SupplierException {
        if ( key == null )
            return null;

        List<Task> tasks = null;

        Matcher matcher = TASKLIST_PATTERN.matcher( context.getContent() );
        while ( matcher.find() ) {
            if ( tasks == null )
                tasks = new java.util.ArrayList<Task>();
            String name = matcher.group( NAME_GROUP );
            String options = matcher.group( OPTIONS_GROUP );
            String body = matcher.group( BODY_GROUP );

            if ( ALL_KEY.equals( key ) || DEFAULT_KEY.equals( key ) && name == null || key.equals( name ) ) {
                addTasks( tasks, options, body );
            }
        }

        return tasks;
    }

    private void addTasks( List<Task> tasks, String options, String body ) {
        // TODO: Ignoring sort options for the moment.

        if ( body == null )
            return;

        List<String> headings = new java.util.ArrayList<String>();
        String[] lines = body.split( "\\r?\\n" );

        for ( String line : lines ) {
            line = line.trim();
            if ( line.length() == 0 )
                // Skip blank lines
                continue;

            Matcher matcher = TASK_PATTERN.matcher( line );

            if ( line.startsWith( "||" ) ) {
                // It's a header row
                headings.clear();
                while ( matcher.find() ) {
                    headings.add( matcher.group( 1 ).trim().toLowerCase() );
                }
            } else {
                Task task = new Task();
                int index = 0;
                while ( index < headings.size() && matcher.find() ) {
                    setValue( task, headings.get( index ), matcher.group( 1 ).trim() );
                    index++;
                }
                tasks.add( task );
            }
        }
    }

    private void setValue( Task task, String heading, String value ) {
        if ( COMPLETED_HEADING.equals( heading ) )
            task.setCompleted( TRUE.equalsIgnoreCase( value ) );
        else if ( PRIORITY_HEADING.equals( heading ) )
            task.setPriority( Task.Priority.forCode( value ) );
        else if ( LOCKED_HEADING.equals( heading ) )
            task.setLocked( TRUE.equalsIgnoreCase( value ) );
        else if ( CREATED_DATE_HEADING.equals( heading ) )
            task.setCreatedDate( toDate( value ) );
        else if ( COMPLETED_DATE_HEADING.equals( heading ) )
            task.setCompletedDate( toDate( value ) );
        else if ( ASSIGNEE_HEADING.equals( heading ) )
            task.setAssignees( toAssignees( value ) );
        else if ( NAME_HEADING.equals( heading ) )
            task.setName( value );
    }

    private List<String> toAssignees( String value ) {
        String[] split = value.split( ",\\s*" );
        return Arrays.asList( split );
    }

    private Date toDate( String milliseconds ) {
        if ( milliseconds == null || milliseconds.trim().length() == 0 )
            return null;

        try {
            return new Date( Long.parseLong( milliseconds ) );
        } catch ( NumberFormatException e ) {
            return null;
        }
    }
}
